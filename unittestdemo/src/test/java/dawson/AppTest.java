package dawson;

import dawson.App;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue(true);
    }

    @Test
    public void testEchoMethod() 
    {
        assertTrue(5 == App.echo(5));
    }
    //assert equals doesnt work so i just used assertTrue instead

    @Test
    public void testOneMoreMethod() 
    {
        assertTrue(5 == App.oneMore(4));
    }
    
}
